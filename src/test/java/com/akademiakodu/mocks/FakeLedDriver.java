package com.akademiakodu.mocks;

public class FakeLedDriver implements LedDriver {
    private boolean state;

    @Override
    public boolean getState() {
        return state;
    }

    @Override
    public void setState(boolean turnedOn) {
        state = turnedOn;
    }
}
