package com.akademiakodu.mocks;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalTime;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class LightsAutomatorTest {
    @Spy
    private LedDriver ledDriver = new FakeLedDriver();
    @Mock
    private SunsetIndicator sunsetIndicator;

    private LightsAutomator lightsAutomator;

    @Before
    public void setUp() {
        lightsAutomator = new LightsAutomator(ledDriver, sunsetIndicator);
    }

    // 1. Czy pobrano date zachodu slonca w odpowiednim momencie.
    @Test
    public void testSunsetTimeFetchedAtCorrectTime() {
        // given
        lightsAutomator.setSunsetFetchTime(LocalTime.of(13, 0));

        // when
        lightsAutomator.onTick(LocalTime.of(13, 0, 1));

        // then
        verify(sunsetIndicator).getSunsetTime();
    }

    // 1a. Czy nie pobierze się godzina zachodu przed godziną w której powinna zostać ona pobrana.
    @Test
    public void testSunsetTimeNotFetchedBeforeCorrectTime() {
        // given
        lightsAutomator.setSunsetFetchTime(LocalTime.of(13, 0));

        // when
        lightsAutomator.onTick(LocalTime.of(12, 55));

        // then
        verify(sunsetIndicator, never()).getSunsetTime();
    }

    // 2. Czy po zachodzie slonca ustawiono LED na włączony.
    // 4. Włączenie LED po zachodzie słońca ma nastąpić tylko i wyłącznie 1 raz.
    @Test
    public void testLedTurnedOnAfterSunset() {
        // given
        lightsAutomator.setSunsetFetchTime(LocalTime.of(13, 0));
        when(sunsetIndicator.getSunsetTime()).thenReturn(LocalTime.of(15, 0));

        // when
        lightsAutomator.onTick(LocalTime.of(13, 0, 1));
        lightsAutomator.onTick(LocalTime.of(15, 0, 1));
        lightsAutomator.onTick(LocalTime.of(15, 0, 2));

        // then
        assertTrue(ledDriver.getState());
        verify(ledDriver, times(1)).setState(true);
    }

    // 3. Czy po czasie autoOff LED zostanie wyłączony.
    @Test
    public void testLedTurnedOffAfterAutoOffTime() {
        // given
        lightsAutomator.setAutoTurnOffTime(LocalTime.of(23, 0));

        // when
        lightsAutomator.onTick(LocalTime.of(23, 0, 1));

        // then
        assertFalse(ledDriver.getState());
        verify(ledDriver).setState(false);
    }

    // 6a. AutoOff = 3:00 i zmiana czasu z 3:00 -> 2:00 - powinno wylaczyc LED o 3:00 nowego czasu
    @Test
    public void testLedTurnedOffByNegativeDSTChange() {
        // given
        lightsAutomator.setAutoTurnOffTime(LocalTime.of(3, 0));

        lightsAutomator.onTick(LocalTime.of(2, 59, 59));
        lightsAutomator.onTick(LocalTime.of(2, 0, 0));

        verify(ledDriver, never()).setState(false);

        lightsAutomator.onTick(LocalTime.of(3, 0, 1));
        verify(ledDriver).setState(false);
    }

    // 6b. AutoOff = 2:00 i zmiana czasu 2:00 -> 3:00 - powinno wyłączyć LED od razu po minieciu
    // 1:59:59 czyli 3:00 nowego czasu
    @Test
    public void testLedTurnedOffByPositiveDSTChange() {
        lightsAutomator.setAutoTurnOffTime(LocalTime.of(2, 0));

        lightsAutomator.onTick(LocalTime.of(1, 59, 59));
        lightsAutomator.onTick(LocalTime.of(3, 0));

        verify(ledDriver).setState(false);
    }

    // 8. Po uruchomieniu przez jakis czas nie udaje sie pobrac godziny zachodu.
    // Godzina zostaje pobrana po czasie zachodu slonca.
    // Automat powinien natychmiast uruchomic LED.
    // Testowa godzina zachodu slonca : 15:00
    @Test
    public void testAfterRunSunsetTimeWasFetchedAfterActualSunset() {
        lightsAutomator.onTick(LocalTime.of(10, 0));
        lightsAutomator.onTick(LocalTime.of(12, 0));
        lightsAutomator.onTick(LocalTime.of(15, 0));
        lightsAutomator.onTick(LocalTime.of(16, 30));

        when(sunsetIndicator.getSunsetTime()).thenReturn(LocalTime.of(15, 0));
        lightsAutomator.onTick(LocalTime.of(17, 0));

        assertTrue(ledDriver.getState());
        verify(ledDriver).setState(true);
    }

    // 8a. Po uruchomieniu nie mozna pobrac godziny az do momentu autoOff i dopiero po nim godizna jest dostepna.
    // LED powinien zostac wylaczony
    // Testowa godzina zachodu slonca : 15:00
    @Test
    public void testAfterRunSunsetTimeWasFetchedAfterAutoOffTime() {
        lightsAutomator.setAutoTurnOffTime(LocalTime.of(21, 0));

        lightsAutomator.onTick(LocalTime.of(12, 0));
        lightsAutomator.onTick(LocalTime.of(15, 0));
        lightsAutomator.onTick(LocalTime.of(18, 0));

        when(sunsetIndicator.getSunsetTime()).thenReturn(LocalTime.of(15, 0));
        lightsAutomator.onTick(LocalTime.of(22, 0));

        verify(ledDriver, never()).setState(anyBoolean());
    }
}