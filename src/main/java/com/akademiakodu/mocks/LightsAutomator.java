package com.akademiakodu.mocks;

import java.time.LocalTime;

public class LightsAutomator {
    private final LedDriver ledDriver;
    private final SunsetIndicator sunsetIndicator;

    private LocalTime sunsetFetchTime = LocalTime.of(12, 0);
    private LocalTime autoTurnOffTime = LocalTime.of(22, 30);

    private LocalTime todaysSunsetTime;
    private boolean todaysAutoTurnOnPerformed;
    private boolean todaysAutoTurnOffPerformed;

    public LightsAutomator(LedDriver ledDriver, SunsetIndicator sunsetIndicator) {
        this.ledDriver = ledDriver;
        this.sunsetIndicator = sunsetIndicator;
    }

    public LocalTime getSunsetFetchTime() {
        return sunsetFetchTime;
    }

    public void setSunsetFetchTime(LocalTime sunsetFetchTime) {
        this.sunsetFetchTime = sunsetFetchTime;
    }

    public LocalTime getAutoTurnOffTime() {
        return autoTurnOffTime;
    }

    public void setAutoTurnOffTime(LocalTime autoTurnOffTime) {
        this.autoTurnOffTime = autoTurnOffTime;
    }

    public void onTick(LocalTime currentTime) {
        if (shouldRefreshSunsetTime(currentTime)) {
            todaysSunsetTime = sunsetIndicator.getSunsetTime();
            todaysAutoTurnOnPerformed = false;
            todaysAutoTurnOffPerformed = false;
        }
        if (shouldTurnLightsOnSunset(currentTime) && shouldAutoTurnOffLights(currentTime)) {
            todaysAutoTurnOffPerformed = true;
            todaysAutoTurnOnPerformed = true;
        }
        if (shouldTurnLightsOnSunset(currentTime)) {
            ledDriver.setState(true);
            todaysAutoTurnOnPerformed = true;
        }
        if (shouldAutoTurnOffLights(currentTime)) {
            ledDriver.setState(false);
            todaysAutoTurnOffPerformed = true;
        }
    }

    private boolean shouldAutoTurnOffLights(LocalTime currentTime) {
        return shouldPerformActionForFlagAndDate(todaysAutoTurnOffPerformed, autoTurnOffTime, currentTime);
    }

    private boolean shouldTurnLightsOnSunset(LocalTime currentTime) {
        return shouldPerformActionForFlagAndDate(todaysAutoTurnOnPerformed, todaysSunsetTime, currentTime);
    }

    private boolean shouldPerformActionForFlagAndDate(boolean executionFlag, LocalTime referenceTime, LocalTime currentTime) {
        return referenceTime != null && !executionFlag && referenceTime.isBefore(currentTime);
    }

    private boolean shouldRefreshSunsetTime(LocalTime currentTime) {
        return sunsetFetchTime.isBefore(currentTime) && todaysSunsetTime == null;
    }
}
