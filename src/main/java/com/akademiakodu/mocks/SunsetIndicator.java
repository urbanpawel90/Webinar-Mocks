package com.akademiakodu.mocks;

import java.time.LocalTime;

public interface SunsetIndicator {
    LocalTime getSunsetTime();
}
