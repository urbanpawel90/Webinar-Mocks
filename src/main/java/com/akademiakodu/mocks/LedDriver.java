package com.akademiakodu.mocks;

public interface LedDriver {
    boolean getState();
    void setState(boolean turnedOn);
}
